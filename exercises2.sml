datatype expr =
	 NUM of int
	 | PLUS of expr * expr
	 | MINUS of expr * expr
	 | TIMES of expr * expr
	 | DIV of expr * expr
	 | F of expr list * (int list -> int);

fun eval (NUM x)            = x
  | eval (PLUS (exp1,exp2))  = eval exp1 + eval exp2
  | eval (MINUS(exp1,exp2)) = eval exp1 - eval exp2
  | eval (TIMES(exp1,exp2)) = eval exp1 * eval exp2
  | eval (DIV  (exp1,exp2)) = eval exp1 div eval exp2
  | eval (F    (expLst,lst)) = lst (map eval expLst);


(* fun flatten [[]] = [] *)
(*     | flatten ([]::xss) = flatten xss *)
(*     | flatten ((x::xs)::xss) = x :: flatten (xs::xss); *)

(* fun flatten2 (a::l) = foldl (op @) a l; *)

fun flatten xs =
    let fun f ([],y) = y
	  | f (x, y) = foldr (op ::) y x
    in
	foldr f [] xs
    end;

fun foldmap f [] = []
  | foldmap f xs = foldr (fn (x,y)=> (f x)::y) [] xs;

fun foldfilter f []  = []
  | foldfilter f xs  = foldr (fn (x,y)=> if f x then x::y else y) [] xs;

fun count f [] = 0
  | count f xs = foldl (fn (x,y) => if f x then y+1 else y) 0 xs;


fun mapPartial checkOpt [] =[]
  | mapPartial checkOpt lst = 
    let fun f (x,y) = case checkOpt x of
			  NONE => y
			| SOME x=>x::y
    in
	foldr f [] lst
    end;
