functor F (M: ORD_MAP where type Key.ord_key = string)
	  (S: ORD_SET where type Key.ord_key = string) :>
	sig
	    val proc: string list -> S.set M.map
	end	   
=
struct
fun proc file_list =
    let fun parseInput ([], map) = map
	  | parseInput ((a::l), map) =
	    let val ins = TextIO.openIn a
		val charList = explode(TextIO.inputAll ins)
		(* TextIO.closeIn ins; *)
				
		fun insertWord (w, tempmap) = case M.find(tempmap,w) of
					NONE => M.insert(tempmap, w, S.add(S.empty,a))
					| _=> M.insert(tempmap,w,S.add(Option.valOf(M.find(tempmap,w)),a))
		fun iterList([], tempword, tempmap) = parseInput(l,insertWord(tempword,tempmap))
		  | iterList ((a::l), tempword, tempmap) =
		    if Char.isSpace(a) then (
			iterList(l,"",insertWord(tempword,tempmap))
		    )
		    else(
			iterList(l,tempword^(Char.toString(a)), tempmap)
		    )
		    
	    in
		iterList(charList, "", map)	   
	    end
    in
	parseInput (file_list, M.empty)
    end	   
end


structure StrSet = RedBlackSetFn(struct type ord_key = string val compare = String.compare end)				
structure StrMap = RedBlackMapFn(struct type ord_key = string val compare = String.compare end)
structure Dictionary = F (StrMap)(StrSet)
val ans = Dictionary.proc ["a.txt", "b.txt"];

(* Test*)
val test_hello =  StrSet.listItems(Option.valOf(StrMap.find(ans, "Hello")));
val test_world = StrSet.listItems(Option.valOf(StrMap.find(ans, "world")));
val test_input = StrSet.listItems(Option.valOf(StrMap.find(ans, "input")));
val test_test  = StrSet.listItems(Option.valOf(StrMap.find(ans, "test")));
val test_a     = StrSet.listItems(Option.valOf(StrMap.find(ans, "a")));
