fun min (a,b) = if a < b then a else b;
fun fib 0 = 0
  | fib 1 = 1
  | fib x = fib (x-1) + fib (x-2);

fun isPrime 2 = true
  | isPrime x = 
    let fun findfactor m =
	    if x mod m = 0 then false
	    else if m*m >= x then true
	    else findfactor(m+1)
    in
	findfactor(2)
    end;	    

fun sumList lst =
    let fun  helper([], ans) = ans
	   | helper(a::l, ans) = helper(l, ans+a)
    in
	helper(lst,0)
    end;
	
fun squareList [] = []
  | squareList (a::l) = (a*a)::(squareList l);
